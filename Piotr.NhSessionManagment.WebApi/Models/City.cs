﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Piotr.NhSessionManagment.WebApi.Models
{
    public class City : Entity
    {
        public virtual long Population { get; set; }
        public virtual string Name { get; set; }
    }
}