using NHibernate.Mapping.ByCode.Conformist;

namespace Piotr.NhSessionManagment.WebApi.Models
{
    public class CityMap : ClassMapping<City>
    {
        public CityMap()
        {
            Id(x => x.Id);
            Property(x => x.Population);
            Property(x => x.Name);
        }
    }
}