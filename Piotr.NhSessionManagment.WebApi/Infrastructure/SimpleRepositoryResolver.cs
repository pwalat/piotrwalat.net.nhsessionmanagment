using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;
using Piotr.NhSessionManagment.WebApi.Models;

namespace Piotr.NhSessionManagment.WebApi.Infrastructure
{
    public class SimpleRepositoryResolver<T> : IDependencyResolver
        where T:Entity
    {
        public void Dispose()
        {
        }

        public object GetService(Type serviceType)
        {
            if(serviceType == typeof(IRepository<T>))
            {
                return new NHibernateRepository<T>()
                           {
                           };
            }
            return null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return new List<object>();
        }

        public IDependencyScope BeginScope()
        {
            return this;
        }
    }
}